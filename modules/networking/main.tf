data "aws_availability_zones" "available" {}

module "vpc" {
  source = "git::https://github.com/terraform-aws-modules/terraform-aws-vpc"

  name                         = "${var.namespace}-vpc"
  cidr                         = var.cidr
  azs                          = data.aws_availability_zones.available.names
  private_subnets              = var.private_subnets
  public_subnets               = var.public_subnets
  database_subnets             = var.database_subnets
  create_database_subnet_group = true
  enable_nat_gateway           = true
  single_nat_gateway           = true
}

module "lb_sg" {
  source = "git::https://github.com/terraform-in-action/terraform-aws-sg"
  vpc_id = module.vpc.vpc_id
  ingress_rules = [{
    port        = 80
    cidr_blocks = ["0.0.0.0/0"]
  }]
}

module "websvr_sg" {

  source = "git::https://github.com/terraform-in-action/terraform-aws-sg"
  vpc_id = module.vpc.vpc_id
  ingress_rules = [
    {
      port            = 8080
      security_groups = [module.lb_sg.security_group.id]
    },
    {
      port        = 22
      cidr_blocks = [var.cidr]

    }
  ]
}

module "db_sg" {
  source = "git::https://github.com/terraform-in-action/terraform-aws-sg"
  vpc_id = module.vpc.vpc_id
  ingress_rules = [{
    port            = 3306
    security_groups = [module.websvr_sg.security_group.id]
  }]
}