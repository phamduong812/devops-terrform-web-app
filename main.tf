module "autoscaling" {
  source    = "./modules/autoscaling"
  namespace = var.namespace
  
  instance_type = var.instance_type
  ssh_keypair   = var.ssh_keypair
  vpc           = module.networking.vpc
  sg            = module.networking.sg
  db_config     = module.database.db_config
}

module "database" {
  source    = "./modules/database"
  namespace = var.namespace
  vpc       = module.networking.vpc
  sg        = module.networking.sg
}

module "s3" {
  source    = "./modules/s3"
  namespace = var.namespace
}

module "networking" {
  source    = "./modules/networking"
  namespace = var.namespace
  cidr                         = "10.0.0.0/16"
  private_subnets              = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets               = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  database_subnets             = ["10.0.21.0/24", "10.0.22.0/24", "10.0.23.0/24"]
}